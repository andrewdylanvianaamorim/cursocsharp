namespace CursoCSharp.Fundamentos
{
    class Comentarios
    {
        public static void Executar()
        {
            ///<summary>
            ///<summary>

            // Cuidado coms os comentários desnecessários
            Console.WriteLine("Código Claro é sempre melhor!");

            Console.WriteLine("C# tem XML Comments!");


            /*
                Esse é um comentário de mais
                de uma linha
            */
            Console.WriteLine("1");
            Console.WriteLine("2");
        }
    }
}
