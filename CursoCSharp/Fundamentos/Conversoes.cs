namespace CursoCSharp.Fundamentos 
{
    class Conversoes
    {
        public static void Executar()
        {
            int inteiro = 10;
            double quedrado;

            quedrado = inteiro;            

            Console.WriteLine(quedrado);

            double nota = 9.7;

            int notaTruncada = (int)nota;

            Console.WriteLine("Nota truncada: {0}", notaTruncada);


            Console.Write("Digite sua idade: ");
            string idadeString = Console.ReadLine();

            int idade = Convert.ToInt32(idadeString);

            Console.WriteLine("Idade inserida: {0}", idade);


            Console.Write("Digite um número: ");
            int.TryParse(Console.ReadLine(),  out int numero);
            Console.WriteLine("Número digitado: {0}", numero);
        }
    }
}
