namespace CursoCSharp
{
    namespace Fundamentos
    {
        class OperadoresAritimeticos
        {
            public static void Executar()
            {
                var preco = 1000.0;
                var imposto = 355;
                var desconto = 0.1;
                double total = preco + imposto;
                var totalComDesconto = total - (total * desconto); 
                Console.WriteLine($"Total com desconto: R${totalComDesconto}");


                //imc
                double peso = 91.2;
                double altura = 1.82;
                double imc = peso / Math.Pow(altura, 2);
                Console.WriteLine("Imc: {0}", imc);

                // número par/ímpar
                int par = 24;
                int impar = 35;
                Console.WriteLine("{0}/2 tem resto {1}", par, par % 2);
                Console.WriteLine("{0}/2 tem resto {1}", impar, impar % 2);
            }
        }
    }
}
