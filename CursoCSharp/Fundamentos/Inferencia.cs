namespace CursoCSharp.Fundamentos
{
    class Inferencia
    {
        public static void Executar()
        {
            var nome = "Andrew";
            Console.WriteLine(nome);
            // nome = 10; <- isso é um erro :(


            var idade = 32;
            Console.WriteLine(idade);


            int a;
            a = 3;

            int b = 2;
            
            Console.WriteLine(a + b);
        }
    }
}
