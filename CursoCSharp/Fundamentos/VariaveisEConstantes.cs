namespace CursoCSharp.Fundamentos
{
    class VariaveisEConstantes
    {
        public static void Executar() 
        {
            // área da circunferencia
            double raio = 4;
            
            const double PI = 3.14159;

            raio = 5.5;
            //PI = 3.14

            double area = PI * raio * raio;
            Console.WriteLine("A Área é " + area);


            // tipos internos

            bool estaChuvendo = true;
            Console.WriteLine("Está chuvendo? " + estaChuvendo);


            byte idade = 45;
            
            Console.WriteLine("Idade " + idade);
            
            sbyte saldoDeGols = sbyte.MinValue;
            Console.WriteLine("Saldo de Goals: " + saldoDeGols);


            short salario = short.MaxValue;
            Console.WriteLine("Salário: R$" + salario);

            int menorValorInteiro = int.MinValue;
            Console.WriteLine("Menor int: " + menorValorInteiro);

            uint populaçãoBrasileira = 207_600_000;
            Console.WriteLine("População Brasileira: " + populaçãoBrasileira);
            
            ulong populaçãoMundial = 7_600_000_000;
            Console.WriteLine("População mundial: " + populaçãoMundial);


            float precoComputador = 1299.99f;
            Console.WriteLine("Preço do computador: R$" + precoComputador);
        
            double valorDeMercadoApple = 1_000_000_000_000.00; // mais usados dos números reais
            Console.WriteLine("Preço da Apple: R$" + valorDeMercadoApple); 


            decimal distanciaEntreAsEstrelas = decimal.MaxValue;
            Console.WriteLine("Distância entre estrelas: " + distanciaEntreAsEstrelas);


            char letra = 'b';
            Console.WriteLine("'b' = " + letra);

            string texto = "Seja bem-vindo ao curso de C#";
            Console.WriteLine(texto);
        }
    }
}
