﻿using System;
using System.Collections.Generic;
namespace CursoCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            new CentralDeExercicios(new Dictionary<String, Action>(){
                {"Primeiro Programa - Fundamentos", Fundamentos.PrimeiroPrograma.Executar},
                {"Comentários - Fundamentos", Fundamentos.Comentarios.Executar},
                {"Variáveis e Constantes - Fundamentos", Fundamentos.VariaveisEConstantes.Executar},
                {"Inferência - Fundamentos", Fundamentos.Inferencia.Executar},
                {"Interpolação - Fundamentos", Fundamentos.Interpolacao.Executar},
                {"Notação ponto - Fundamentos", Fundamentos.NotacaoPonto.Executar},
                {"Lendo dados - Fundamentos", Fundamentos.LendoDados.Executar},
                {"Formatando Números - Fundamentos", Fundamentos.FormatandoNumero.Executar},
                {"Conversões - Fundamentos", Fundamentos.Conversoes.Executar},
                {"Operadores aritimeticos - Fundamentos", Fundamentos.OperadoresAritimeticos.Executar},
                {"Operadores relacionais - Fundamentos", Fundamentos.OperadoresRelacionais.Executar},
            }).SelecionarEExecutar();
        }
    }
}
